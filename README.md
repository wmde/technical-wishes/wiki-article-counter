# Uses the MediaWiki API to compile a list of current article counts for each Wikipedia

See the included [output file](./wiki_article_counts.csv)

## Running

To update the results, install Elixir and Livebook, then:

```
livebook server wiki_article_counts.livemd
```
